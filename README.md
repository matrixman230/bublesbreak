# **Bubles Break** #
Small simple pong-style game. Created in [Godot 2](http://www.godotengine.org/projects/godot-engine). I created this game as example for my tutorials.

## **Build From Source Codes** ##
* Clone repository
* Download [Godot (>=2.0)](http://www.godotengine.org/projects/godot-engine)
* Download Source Code from [Download](https://bitbucket.org/CDmir230/bublesbreak/downloads) 
* Run Godot
* Push Import Button
* Set path to '**your_path/BublesBreak/src/engine.cfg**'
* Run or Edit